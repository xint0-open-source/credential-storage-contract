# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/xint0-open-source/credential-storage-contract/-/compare/1.0.0...main)

### Changed

- Add phpstan to project.
- Add phpcs and phpcbf to project.
- Update phive volume mapping to /root/.phive in docker compose php service.
- Add php 8.4 to ci job execution matrix.

## [1.0.0](https://gitlab.com/xint0-open-source/credential-storage-contract/-/compare/0.0.4...1.0.0) - 2024-08-29

### Added

- `Xint0\CredendialStorage\Exceptions\CredentialNotFoundException`.

## [0.0.4](https://gitlab.com/xint0-open-source/credential-storage-contract/-/compare/0.0.3...0.0.4) - 2023-01-30

### Changed
- `CredentialInterface@getSecret` returns encrypted secret, add `CredentialInterface@decryptSecret` to return decrypted
secret.

## [0.0.3](https://gitlab.com/xint0-open-source/credential-storage-contract/-/compare/0.0.2...0.0.3) - 2023-01-30

### Changed
- Do not export `.idea` directory.

## [0.0.2](https://gitlab.com/xint0-open-source/credential-storage-contract/-/compare/0.0.1...0.0.2) - 2023-01-30

### Added
- Extract `createCredential` method to `CredentialFactoryInterface`.
- Add `CredentialFactoryExceptionInterface`.

### Changed
- Rename `CredentialStoreException` to `CredentialStoreExceptionInterface`.

## [0.0.1](https://gitlab.com/xint0-open-source/credential-storage-contract/-/tags/0.0.1) - 2023-01-19

### Added 
- Initial release with definitions for `CredentialInterface`, `CredentialStoreException`, and
`CredentialStoreInterface` contracts.