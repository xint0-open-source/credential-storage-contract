<?php

/**
 * @copyright 2024 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/credential-storage-contract/-/blob/main/LICENSE MIT
 */

declare(strict_types=1);

namespace Tests\Unit\Exceptions;

use Exception;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Xint0\CredentialStorage\Exceptions\CredentialNotFoundException;

#[CoversClass(CredentialNotFoundException::class)]
class CredentialNotFoundExceptionTest extends TestCase
{
    public function test_new_instance_has_expected_values(): void
    {
        $actual = new CredentialNotFoundException('credential_name');

        $this->assertSame('Credential "credential_name" not found.', $actual->getMessage());
        $this->assertSame(0, $actual->getCode());
        $this->assertNull($actual->getPrevious());
        $this->assertSame('credential_name', $actual->getCredentialName());
    }

    public function test_new_instance_with_code_has_expected_values(): void
    {
        $code = 10;

        $actual = new CredentialNotFoundException('credential_name', $code);

        $this->assertSame('Credential "credential_name" not found.', $actual->getMessage());
        $this->assertSame($code, $actual->getCode());
        $this->assertNull($actual->getPrevious());
        $this->assertSame('credential_name', $actual->getCredentialName());
    }

    public function test_new_instance_with_previous_exception_has_expected_values(): void
    {
        $previous = new Exception();

        $actual = new CredentialNotFoundException('credential_name', 0, $previous);

        $this->assertSame('Credential "credential_name" not found.', $actual->getMessage());
        $this->assertSame(0, $actual->getCode());
        $this->assertSame('credential_name', $actual->getCredentialName());
        $this->assertSame($previous, $actual->getPrevious());
    }
}
