<?php

/**
 * xint0/credential-storage-contract
 *
 * Credendital storage contract.
 *
 * @author Rogelio Jacinto Pascual <rogelio.jacinto@gmail.com>
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/credential-storage-contract/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\CredentialStorage\Contracts;

use Throwable;

/**
 * An exception that must be thrown when a
 * \Xint0\CredentialStorage\Contracts\CredentialFactoryInterface implementation
 * encounters an error condition.
 */
interface CredentialFactoryExceptionInterface extends Throwable
{
}