<?php

/**
 * xint0/credential-storage-contract
 *
 * Credendital storage contract.
 *
 * @author Rogelio Jacinto Pascual <rogelio.jacinto@gmail.com>
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/credential-storage-contract/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\CredentialStorage\Contracts;

interface CredentialFactoryInterface
{
    /**
     * Create a new credential.
     *
     * @param  string  $identity
     * @param  string  $secret
     *
     * @return CredentialInterface
     *
     * @throws CredentialFactoryExceptionInterface When the credential factory encounters
     * an error that prevents credential creation.
     */
    public function createCredential(string $identity, string $secret): CredentialInterface;
}