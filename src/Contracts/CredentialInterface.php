<?php

/**
 * xint0/credential-storage-contract
 *
 * Credendital storage contract.
 *
 * @author Rogelio Jacinto Pascual <rogelio.jacinto@gmail.com>
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/credential-storage-contract/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\CredentialStorage\Contracts;

/**
 * A credential consisting of identity and a secret.
 *
 * The secret part of the credential is encrypted when the credential is stored.
 */
interface CredentialInterface
{
    /**
     * Returns the decrypted secret part of the credential.
     *
     * @return string
     */
    public function decryptSecret(): string;

    /**
     * Returns the public identity of the credential.
     *
     * @return string
     */
    public function getIdentity(): string;

    /**
     * Returns the encrypted secret part of the credential.
     *
     * @return string
     */
    public function getSecret(): string;
}