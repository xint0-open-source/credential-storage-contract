<?php

/**
 * xint0/credential-storage-contract
 *
 * Credendital storage contract.
 *
 * @author Rogelio Jacinto Pascual <rogelio.jacinto@gmail.com>
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/credential-storage-contract/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\CredentialStorage\Contracts;

/**
 * A credential store.
 *
 * Includes methods for storing and retrieving credentials.
 */
interface CredentialStoreInterface
{
    /**
     * Retrieves a credential.
     *
     * @param  string  $name
     *
     * @return CredentialInterface
     *
     * @throws CredentialStoreExceptionInterface When the credential store encounters an
     * error and cannot retrieve the credential.
     */
    public function getCredential(string $name): CredentialInterface;


    /**
     * Saves existing credential in store.
     *
     * @param  string  $name
     * @param  CredentialInterface  $credential
     *
     * @return void
     *
     * @throws CredentialStoreExceptionInterface When the credential store encounters an
     * error and cannot save the credential.
     */
    public function putCredential(string $name, CredentialInterface $credential): void;
}