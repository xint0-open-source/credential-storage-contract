<?php

/**
 * @copyright 2024 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/credential-storage-contract/-/blob/main/LICENSE MIT
 */

declare(strict_types=1);

namespace Xint0\CredentialStorage\Exceptions;

use RuntimeException;
use Throwable;
use Xint0\CredentialStorage\Contracts\CredentialStoreExceptionInterface;

/**
 * Thrown when the credential is not found in the store.
 *
 * @author Rogelio Jacinto <rogelio.jacinto@gmail.com>
 */
class CredentialNotFoundException extends RuntimeException implements CredentialStoreExceptionInterface
{
    public function __construct(private readonly string $credential_name, int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct(sprintf('Credential "%s" not found.', $this->credential_name), $code, $previous);
    }

    /**
     * Returns the name of the credential that was not found.
     *
     * @return string The credential name that was not found.
     */
    public function getCredentialName(): string
    {
        return $this->credential_name;
    }
}