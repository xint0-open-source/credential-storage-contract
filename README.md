# Credential Storage Contract

![Packagist Version](https://img.shields.io/packagist/v/xint0/credential-storage-contract)
![Packagist Downloads](https://img.shields.io/packagist/dt/xint0/credential-storage-contract)
![GitLab License](https://img.shields.io/gitlab/license/xint0-open-source%2Fcredential-storage-contract)
![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/xint0-open-source%2Fcredential-storage-contract)
![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/xint0-open-source%2Fcredential-storage-contract)
![Packagist Dependency Version](https://img.shields.io/packagist/dependency-v/xint0/credential-storage-contract/php)


## Description

Defines contracts for storing and retrieving credentials with encrypted secret.

The contracts only define methods for creating, storing, and retrieving of credentials (identity and secret) and do not
concern with the actual implementation or define a specific cryptographic requirements.

## Installation

```
composer require xint0/credential-storage-contract
```

## Usage

The idea behind this package is to have a standard abstraction for creating, storing, and retrieving credentials.

Implementations are responsible for the actual storing and securing of credentials.

A mock implementation is available as [xint0/mock-credential-store](https://packagist.org/packages/xint0/mock-credential-store)
that can be useful for testing purposes.

## Support

Create issue in gitlab: https://gitlab.com/xint0-open-source/credential-storage-contract/-/issues

## Contributing

Fork the project in gitlab and create a merge request.

## License

This project is open source project licensed under the MIT license.
